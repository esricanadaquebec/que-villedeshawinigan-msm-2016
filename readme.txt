﻿Projet de modification du site Mes Services Municipaux permettant aux citoyens de la ville de consulter les informations tels: la date prochaine de la collecte des ordures.

Utiliser le site suivant pour Encoder l'url:
http://www.urlencoder.org/


Test data:

4100 Boulevard Royal, Shawinigan
http://localhost:63728/index.htm?civicaddress=4100%20Boulevard%20Royal,%20Shawinigan
ou
http://localhost:63728/index.htm?civicaddress=4100 Boulevard Royal, Shawinigan


1120 10e Rue, Shawinigan
http://localhost:63728/index.htm?civicaddress=1120%2010e%20Rue%2C%20Shawinigan

870 8e Rue, Shawinigan, Québec, G9T
http://localhost:63728/index.htm?civicaddress=870%208e%20Rue%2C%20Shawinigan



Pour tester la modification cernant les jours fériés, il faut ajuster le contenu de la table Holidays en ajustant la valeur pour 
Double Back ou Double Forw pour Doubler le travail le lendemain ou le jour précédent une journée fériée.



