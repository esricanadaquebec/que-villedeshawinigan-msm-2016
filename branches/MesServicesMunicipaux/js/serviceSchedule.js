﻿//---Create Regular Schedule for full year-------------------------------|
function parseScheduleDates(schedule, type, year) {
    var result = [];
    //---DAILY----------------------------------|
    if (type == "Daily") {
        var d = new Date();
        d.setFullYear(year); d.setMonth(0); d.setDate(1); d.setHours(0, 0, 0, 0);
        if (dojo.date.isLeapYear(d)) { var numDays = 366; }
        else { var numDays = 365; }
        for (i = 0; i < numDays; i++) {
            result.push(dojo.date.add(d, "day", i));
        }
    }

        //---WEEKLY CYCLES--------------------------|
    else if (type == "Weekly" || type == "2 Week" || type == "3 Week" || type == "4 Week" || type == "5 Week") {
        var d;
        var dates = [];
        var weekdays = schedule.split('');
        //Set cycle
        switch (type) {
            case "Weekly": cycle = 1; break;
            case "2 Week": cycle = 2; break;
            case "3 Week": cycle = 3; break;
            case "4 Week": cycle = 4; break;
            case "5 Week": cycle = 5; break;
            default: cycle = 1; break;
        }
        dojo.forEach(weekdays, function (item, idx) {
            if (item == "0") { return; } //Move to next day if not scheduled
            d = new Date();
            d.setFullYear(year); d.setMonth(0); d.setDate(1); d.setHours(0, 0, 0, 0);

            //Determine first date of cycle
            var gap = idx - d.getDay();
            if (gap < 0 && item == 1) { d = dojo.date.add(d, "week", cycle); }
            d = dojo.date.add(d, "day", gap);
            d = dojo.date.add(d, "week", parseInt(item) - 1);

            //Create all dates
            for (var i = 0; i <= 52; i += cycle) {
                var nd = dojo.date.add(d, "week", i);
                if (nd.getFullYear() == year) { dates.push(new Date(nd)); }
            }
        });
        result = sortDateArray(dates);
    }

        //---MONTHLY--------------------------------|
    else if (type == "Monthly") {
        for (var i = 0; i < 12; i++) {
            var d = new Date();
            d.setFullYear(year); d.setMonth(i); d.setDate(1); d.setHours(0, 0, 0, 0);
            if (schedule.length > 2) { //nth weekday of the month
                var weekdays = schedule.split('');
                dojo.forEach(weekdays, function (item, idx) {
                    if (item == "0") { return; } //Move to next day if not scheduled
                    else if (item == "9") { //Look for last day of the week in month
                        d.setDate(dojo.date.getDaysInMonth(d));
                        var gap = d.getDay() - idx;
                        if (gap < 0) { gap += 7 }
                        d = dojo.date.add(d, "day", gap * -1);
                        result.push(new Date(d));
                    }
                    else {
                        //Determine first occurence of weekday
                        d.setDate(1);
                        var gap = idx - d.getDay();
                        if (gap < 0) { gap += 7; }
                        d = dojo.date.add(d, "day", gap);
                        d = dojo.date.add(d, "week", parseInt(item) - 1);
                        result.push(new Date(d));
                    }
                });
            }
            else { //nth day of the month
                d.setDate(parseInt(schedule));
                result.push(new Date(d));
            }
        }
    }
        //---ANNUAL/IRREGULAR/SEASONALLY------------|
    else if (type == "Annually" || type == "Irregularly" || type == "Seasonally") {
        var breakdown = schedule.split(",");
        dojo.forEach(breakdown, function (item, i) {
            var d = new Date(); d.setHours(0, 0, 0, 0);
            var dates = item.split("-");
            d.setFullYear(parseInt(dates[0]));
            d.setMonth(parseInt(dates[1]) - 1);
            d.setDate(parseInt(dates[2]));
            result.push(new Date(d));
        });
    }
    return result;
}

//---Create Schedule Pattern Description-------------------------------|
function schedulePatternDescriptor(schedule, type) {

    var result = "";
    var weekdays = intl.weekdays;
    var terms = intl.terms;

    if (type == "Monthly" && schedule.length <= 2) {
        var idx = Number(schedule);
        if (idx <= 5 && idx > 0) {
            result = terms[idx] + " " + terms[9];
        }
        else {
            result = String(idx) + terms[6] + " " + terms[9];
        }
    }
    else if (type == "Weekly" || type == "2 Week" || type == "3 Week" || type == "4 Week" || type == "5 Week" || type == "Monthly") {
        dojo.forEach(schedule.split(""), function (item, i) {
            if (item != "0") {
                if (result != "") { result += " " + terms[0] + " "; }
                if (type == "Weekly") {
                    result += weekdays[i];
                }
                else if (type == "2 Week" || type == "3 Week" || type == "4 Week" || type == "5 Week") {
                    result += terms[7] + " " + String(item) + ": " + weekdays[i];
                }
                else if (type == "Monthly" && schedule.length > 2) { //nth weekday of the month
                    result += terms[item] + " " + weekdays[i] + " " + terms[8];
                }
            }
        });
    }

    if (result != "") { result = " | " + result };
    return result;
}

//---Adjust Regular Schedule around holidays-----------------------------|
function modForHolidays(regSched, holidayArray) {

    //Loop through each schedule day
    var modSched = dojo.map(regSched, function (sched, si) {
        var tempSched = sched; //will store the date, until modified
        //Loop through each holiday
        dojo.forEach(holidayArray, function (holiday, hi) {
            //--SHIFT FORWARD/BACK--------------|
            if (holiday[0] == "Shift Forw" || holiday[0] == "Shift Back") {
                dFr = dojo.date.add(holiday[1], "day", -1 * holiday[1].getDay());
                dTo = dojo.date.add(holiday[1], "day", 6 - holiday[1].getDay());
                if (sched.getTime() >= dFr.getTime() && sched.getTime() <= dTo.getTime()) {
                    if (holiday[0] == "Shift Forw" && sched.getTime() >= holiday[1].getTime()) {
                        tempSched = dojo.date.add(sched, "day", 1);
                    }
                    else if (holiday[0] == "Shift Back" && sched.getTime() <= holiday[1].getTime()) {
                        tempSched = dojo.date.add(sched, "day", -1);
                    }
                }
            }

                ///////////////////////////////////////////////////////////////////////////////////////////////////////

                //Modif pour Doubler le travail le lendemain ou le jour précédent une journée fériée

            else if (holiday[0] == "Double Back" && holiday[1].getTime() == sched.getTime()) {
                tempSched = dojo.date.add(sched, "day", -1);
            }

            else if (holiday[0] == "Double Forw" && holiday[1].getTime() == sched.getTime()) {
                tempSched = dojo.date.add(sched, "day", 1);
            }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////

            else if (holiday[1].getTime() == sched.getTime()) {
                switch (holiday[0]) {
                    //--NEXT SAT----------------|
                    case "Next Sat":
                        if (sched.getDay() == 6) {
                            tempSched = dojo.date.add(sched, "day", 7);
                        }
                        else {
                            tempSched = dojo.date.add(sched, "day", 6 - sched.getDay());
                        }
                        break;
                        //--PREV SAT----------------|
                    case "Prev Sat":
                        tempSched = dojo.date.add(sched, "day", (sched.getDay() * -1) - 1);
                        break;
                        //--NEXT SUN----------------|
                    case "Next Sun":
                        tempSched = dojo.date.add(sched, "day", 7 - sched.getDay());
                        break;
                        //--PREV SUN----------------|
                    case "Prev Sun":
                        if (sched.getDay() == 0) {
                            tempSched = dojo.date.add(sched, "day", -7);
                        }
                        else {
                            tempSched = dojo.date.add(sched, "day", sched.getDay() * -1);
                        }
                        break;
                        //--CANCEL------------------|
                    case "Cancel":
                        tempSched = null;
                        break;
                }
            }
        });
        return tempSched;
    });
    return dojo.filter(modSched, function (item) { return item != null; });
}